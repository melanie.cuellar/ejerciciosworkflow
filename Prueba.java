package mx.com.ids.beca.ejercicio4;

public class Prueba {

	public static void main(String[] args) {
		Transaccion d = new Deposito();
		Transaccion p = new Retiro();
		//Transaccion t = new Transaccion(); //esto no debe compilar 
		Transaccion t;
		p.log();
		d.log();
		System.out.println("--------");
		t = p; 
		t.log(); 
		System.out.println("--------");
		t = d; 
		t.log();

	}

}
